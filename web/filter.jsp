<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="model.Patient" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Boostrap CSS only -->
        <!--
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> -->
        <!--VENDORS-->
        <link rel="stylesheet" href="vendors/bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <script src="vendors/jquery/jquery-3.3.1.min.js"></script>
        <!--CONTROL-->
        <script type="text/javascript" src="./js/control/utils.js"></script>
        <script type="text/javascript" src="./js/control/index.js"></script>
        <title>PORTAL PT152 - PATIENTS FILTER</title>
    </head>
    <body>
        <%  
            List<Patient> patientsList = new ArrayList<Patient>();
            if(session.getAttribute("user")==null){
                response.sendRedirect("login.jsp");
            } else {
                // Carrega la llista de pacients.
                // Important: Tant si es una llista completa o filtrada.
                if(request.getAttribute("patientsList")!=null) {
                    patientsList = (List<Patient>) request.getAttribute("patientsList"); 
                } else {
                    // Cridem al servlet de Llibres, 
                    // mètode que carrega la llista de Pacients des de 0.
                    response.sendRedirect("Patients?action=List");
                }
            }
        %>
        <header>
            <%@include file="menu.jsp" %>
        </header>
        <main class="container">
            <h3>FILTER PATIENTS, CLASSIFICACIÓ DE L'ESTUDI.</h3>
            <!-- Formulari de filtre de pacients -->
            <form class="form-inline" id="filter_form" method="post" action="Patients">
                <div class="form-group row">
                    <label class="input-group-prepend col-sm-2" for="classfication_filter">
                        Classification
                    </label>

                    <select class="form-control custom-select col-sm-3" name="category" id="category">
                        <option value="-">---</option>
                        <option value="NORMAL">NORMAL</option>
                        <option value="OSTEOPENIA">OSTEOPENIA</option>
                        <option value="OSTEOPOROSI">OSTEOPOROSI</option>
                    </select>
                    
                    <label class="input-group-prepend col-sm-2" for="classfication_filter">
                        Menopause Type
                    </label>

                    <select class="form-control custom-select col-sm-3" name="menopauseType" id="menopauseType">
                        <option value="-">---</option>
                        <option value="NATURAL">NATURAL</option>
                        <option value="NO CONSTA">NO CONSTA</option>
                    </select>
                    <button type="submit" form="filter_form" 
                            class="btn btn-primary col-sm-2" 
                            name="action" value="filterPatients">Filter</button>
                </div>
            </form>
            <!-- Llistat de pacients -->
            <table class="table table-striped">    
                <thead>
                    <th scope='col'>Register ID</th>
                    <th scope='col'>Age</th>
                    <th scope='col'>Age Group</th>
                    <th scope='col'>Weight</th>
                    <th scope='col'>Height</th>
                    <th scope='col'>IMC</th>
                    <th scope='col'>Classification</th>
                    <th scope='col'>Menarche</th>
                    <th scope='col'>Menopause Type</th>
                </thead>
                <%
                    for(Patient pati: patientsList) {
                %>
                <tr>
                    <td width="10%">
                        <%=pati.getRegisterId()%>
                    </td>
                    <td width="10%">
                        <%=pati.getAge()%>
                    </td>
                    <td width="10%">
                        <%=pati.getAgeGroup()%>
                    </td>
                    <td width="10%">
                        <%=pati.getWeight()%>
                    </td>
                    <td width="10%">
                        <%=pati.getHeight()%>
                    </td>
                    <td width="10%">
                        <%=pati.getImc()%>
                    </td>
                    <td width="10%">
                        <%=pati.getClassification()%>
                    </td>
                    <td width="10%">
                        <%=pati.getMenarche()%>
                    </td>
                    <td width="10%">
                        <%=pati.getMenopauseType()%>
                    </td>
                </tr>
            <%
                }
            %>
            </table>
        </main>
        <footer>
               Institut Provençana 2020-2021, CC-BY.
        </footer>
    </body>
</html>