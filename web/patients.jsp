<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="model.Patient" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Boostrap CSS only -->
        <!--
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> -->
        <!--VENDORS-->
        <link rel="stylesheet" href="vendors/bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <script src="vendors/jquery/jquery-3.3.1.min.js"></script>
        <!--CONTROL-->
        <script type="text/javascript" src="./js/control/utils.js"></script>
        <script type="text/javascript" src="./js/control/index.js"></script>
        <title>PORTAL PT152 - PATIENTS LIST</title>
    </head>
    <body>
        <%  
            List<Patient> patientsList = new ArrayList<Patient>();
            if(session.getAttribute("user")==null){
                response.sendRedirect("login.jsp");
            } else {
                // Carrega la llista de pacients si està buida.
                if(request.getAttribute("patientsList")!=null) {
                    patientsList = (List<Patient>) request.getAttribute("patientsList"); 
                } else {
                    // Cridem al servlet de Llibres, 
                    // mètode que carrega la llista de Pacients des de 0.
                    response.sendRedirect("Patients?action=List");
                }
            }
        %>
        <header>
            <%@include file="menu.jsp" %>
        </header>
        <main class="container">
            <h3>PATIENTS LIST</h3>
            <!-- en forma de grid -->
            <div class="row">   
            <%
                for(Patient patient: patientsList) {
            %>
               <section height="100" class="col-md-4 col-sm-6 col-xs-12">
                   <%  // Alguns camps només els podrà veure l'admin!
                       if (session.getAttribute("role").equals("ADMIN")) {
                   %>        
                   <p><strong>RegId</strong>
                       <%=patient.getRegisterId()%></p>
                   <p><strong>Edat</strong>
                       <%=patient.getAge()%></p>
                   <% } %>
                   <p><strong>Age Group</strong>
                       <%=patient.getAgeGroup()%></p>
                    <p><strong>Classification</strong>
                        <%=patient.getClassification()%></p>
                    <p><strong>Menopausia</strong>
                        <%=patient.getMenarche()%></p>
                    <p><strong>Tipus de Menopausia</strong>
                        <%=patient.getMenopauseType()%></p>
               </section>
            <%
              }
            %>  
            </div>
        </main>
        <footer>
               
        </footer>
    </body>
</html>
