## Projecte Java Web. Servlets, MVC, JSP, fitxers CSV.

⚠ Projecte Desactualitzat des del maig del 2021. No es pot garantir que funcioni ⚠

En cas de voler-lo instal·lar, necessitareu:
- Apache Netbeans 11
- Tomcat 9.0.20 (amb versions superiors dona errors)
- JDK 11 (hauria de funcionar amb versions superiors també) i JDK 8. Es requereixen les dues JDK per a què funcioni Tomcat i els Servlets.
- Les llibreries incloses a WEB-INF/lib haurien de ser suficients.

### Volem crear una web especialitzada en l'estudi de l'osteoporosi. 
La OMS ens han encarregat una web per a gestionar el llistat de les pacients, només per a usuaris investigadors. 

Aquest llistat sortirà de les dades guardades a la taula de nom osteoporosi.csv amb els següents camps:

- idRegistre 
: És un número enter incremental
- edat 
: És un número enter	
- grupEdat 
: Ha de ser un d'aquests intervals: fins a 45, 45-49, 50-54, 55-59, 60-64, 65-69, més de 69
- pes i talla han d'estar donats en números enters. 
: El pes està donat en kilograms i la talla en centímetres.
- imc :
És l'index de Massa Corporal
- classificació 
: És un valor entre els següents termes: osteopenia,normal,osteoporosi
- menarquia: és l'edat de la primera regla (número enter)
- menopausia: És SI o NO
- tipusMenopausia : és un valor entre els següents termes: no consta, natural, ovariectomia, histeroctomia,ambdues

[Enllaç exemples de registres de la taula osteoporosi.csv](https://gitlab.com/m15-uf1-pt15-pacientsoestoporosi/pt15-oestoporosi-javaweb/-/blob/master/web/WEB-INF/files/osteoporosi.csv)

