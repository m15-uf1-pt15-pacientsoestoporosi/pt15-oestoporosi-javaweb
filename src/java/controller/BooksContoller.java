package controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import javax.servlet.annotation.WebServlet;
import model.Book;
import model.BooksManager;


@WebServlet(name = "BooksContoller", urlPatterns = {"/Books"})
public class BooksContoller extends HttpServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session=request.getSession();
        if(session.getAttribute("user")==null){
            response.sendRedirect("login.jsp"); 
        } else{
           
            BooksManager booksManager = new BooksManager();
            
            // Si la category esta buida
            // llegir el llistat complet de llibres.
            List<Book> result = booksManager.buscarTodos();
            
            // Si l'usuari ha seleccionat una categoria, filtrem la llista de llibres.
            if(request.getParameter("action")!=null){
                String action = request.getParameter("action");
                String category = request.getParameter("category");
	
                if (action.equals("filter") && category!= null && !category.equals("") ) {
                    result = booksManager.buscarPorCategoria(category);
                }
            }   
            // Pintem la llista de llibres (completa o filtrada) a la jsp
            request.setAttribute("booksList", result);
            
            RequestDispatcher view = request.getRequestDispatcher("books.jsp");
            // Use the request dispatcher to ask the Container to crank up the JSP, 
            // sending it the request and response.
            view.forward(request, response);	
        }
        
    }
    
    
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
}