package model;

public class Book {

    private String isbn;
    private String titulo;
    private String categoria;
    private String autoras;
    private String formato;
        
    public String getAutoras() {
        return autoras;
    }

    public void setAutoras(String autoras) {
        this.autoras = autoras;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }
	
        
        
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}


	public Book() {
		super();
	}

	public Book(String isbn, String titulo, String categoria, String autoras, String formato) {
		super();
		this.isbn = isbn;
		this.titulo = titulo;
		this.categoria = categoria;
                this.autoras = autoras;
                this.formato = formato;
	}
        
}
